<?php

namespace App\Models;

/**
 * Class Icons
 */
class Icons
{
    public const TEAM_VALOR = "\u{2764}",
        TEAM_INSTINCT = "\u{1F49B}",
        TEAM_MYSTIC = "\u{1F499}",
        TEAM_NONE = "\u{1F5A4}",
        GYM_EX = "\u{2B50}",
        LOCATION = "\u{1F4CD}",
        CHECK_MARK = "\u{2705}",
        RED_CROSS = "\u{274C}";
}
