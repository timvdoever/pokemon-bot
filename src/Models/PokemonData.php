<?php

namespace App\Models;

/**
 * Class PokemonData
 */
class PokemonData
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $form;

    /**
     * @var array
     */
    private $forms;

    /**
     * @var array
     */
    private $types;

    /**
     * @var array
     */
    private $weather;

    /**
     * @var array
     */
    private $CPs;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getForm(): ?string
    {
        return $this->form;
    }

    /**
     * @param string|null $form
     */
    public function setForm(?string $form): void
    {
        if (null === $form) {
            $this->form = 'normal';
        } else {
            $this->form = strtolower($form);
        }
    }

    /**
     * @return array
     */
    public function getForms(): array
    {
        return $this->forms;
    }

    /**
     * @param array $forms
     */
    public function setForms(array $forms): void
    {
        $this->forms = $forms;
    }

    /**
     * @param string $type
     */
    public function addType(string $type): void
    {
        $this->types[] = $type;
    }

    /**
     * @param array $types
     *
     * @return array
     */
    public function setTypes(array $types): array
    {
        foreach ($types as $type) {
            $this->types[] = $type;
        }
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @return array
     */
    public function getWeather(): array
    {
        return $this->weather;
    }

    /**
     * @param array $weather
     */
    public function setWeather(array $weather): void
    {
        $this->weather = $weather;
    }

    /**
     * @return array
     */
    public function getCPs(): array
    {
        return $this->CPs;
    }

    /**
     * @param string $type
     *
     * @return string|null
     */
    public function getCP(string $type): ?string
    {
        if (!array_key_exists($type, $this->CPs)) {
            return null;
        }

        return $this->CPs[$type];
    }

    /**
     * @param array $CPs
     */
    public function setCPs(array $CPs): void
    {
        $this->CPs = $CPs;
    }
}
