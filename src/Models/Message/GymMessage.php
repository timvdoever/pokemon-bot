<?php

namespace App\Models\Message;

use App\Entity\Gym;
use App\Repository\GymRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class GymMessage
 */
class GymMessage extends AbstractMessage
{
    /**
     * @var GymRepository
     */
    private $gymRepository;

    /**
     * GymMessage constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param Environment            $twig
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Environment $twig
    ) {
        parent::__construct($translator, $twig);

        $this->gymRepository = $entityManager->getRepository(Gym::class);
    }

    /**
     * Get the main view of the Gym message flow
     *
     * @return array Sends an array with the reply and an optional keyboard attached
     */
    public function getMainView(): array
    {
        $reply = $this->translator->trans('pick_gym_letter');
        $gyms = $this->gymRepository->findBy(['enabled' => true], ['name' => 'ASC']);
        $this->keyboardGenerator->clear();

        $hiddenGyms = $this->gymRepository->findBy(['enabled' => false]);
        if (0 !== count($hiddenGyms)) {
            $this->keyboardGenerator->addButton($this->translator->trans('hidden_gyms'), 'gym:view:list:hidden');
        }

        $this->addFirstCharacters($gyms, 'gym:view:list');

        $this->keyboardGenerator->addFooter('gym:delete');

        return [$reply, $this->keyboardGenerator->generate()];
    }

    /**
     * Get a sub view of the Gym message flow
     *
     * @param string $view
     * @param string $identifier
     *
     * @return array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getSubView(string $view, string $identifier): array
    {
        $this->keyboardGenerator->clear();

        switch ($view) {
            case 'list':
                return $this->getGymListView($identifier);
            case 'detail':
                return $this->getGymDetailView($identifier);
        }
    }

    /**
     * Get a list view of the gyms
     *
     * @param string $identifier
     *
     * @return array
     */
    private function getGymListView(string $identifier): array
    {
        $reply = $this->translator->trans('pick_gym');

        if ('hidden' === $identifier) {
            $gyms = $this->gymRepository->findBy(['enabled' => false]);
        } else {
            $gyms = $this->gymRepository->findByFirstCharacter($identifier);
        }

        foreach ($gyms as $gym) {
            $this->keyboardGenerator->addButton($gym->getName(), 'gym:view:detail:' . $gym->getId());
        }

        $this->keyboardGenerator->addFooter('gym:delete', 'gym:init');

        return [$reply, $this->keyboardGenerator->generate()];
    }

    /**
     * Get a detailed view of a particular gym
     *
     * @param string $identifier
     *
     * @return array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function getGymDetailView(string $identifier): array
    {
        $gym = $this->gymRepository->find($identifier);

        $reply = $this->twig->render('gym_detail.html.twig', ['gym' => $gym]);

        if ($gym->getEnabled()) {
            $this->keyboardGenerator->addButton($this->translator->trans('disable'), 'gym:update:enabled_false:' . $gym->getId());
        } else {
            $this->keyboardGenerator->addButton($this->translator->trans('enable'), 'gym:update:enabled_true:' . $gym->getId());
        }

        if ($gym->getEx()) {
            $this->keyboardGenerator->addButton($this->translator->trans('unmark_ex'), 'gym:update:ex_false:' . $gym->getId());
        } else {
            $this->keyboardGenerator->addButton($this->translator->trans('mark_ex'), 'gym:update:ex_true:' . $gym->getId());
        }

        $this->keyboardGenerator->addButton($this->translator->trans('remove'), 'gym:update:drop_confirm:' . $gym->getId());

        if ($gym->getEnabled()) {
            $this->keyboardGenerator->addFooter('gym:delete', 'gym:view:list:' . mb_substr($gym->getName(), 0, 1));
        } else {
            $this->keyboardGenerator->addFooter('gym:delete', 'gym:view:list:hidden');
        }

        return [$reply, $this->keyboardGenerator->generate()];
    }
}
