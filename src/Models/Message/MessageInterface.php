<?php

namespace App\Models\Message;

/**
 * Interface MessageInterface
 */
interface MessageInterface
{
    /**
     * The main message to be called by commands
     *
     * @return array
     */
    public function getMainView(): array;
}
