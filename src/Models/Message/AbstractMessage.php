<?php

namespace App\Models\Message;

use App\Entity\Gym;
use App\Generator\InlineKeyboardMarkupGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class AbstractMessage
 */
abstract class AbstractMessage implements MessageInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var InlineKeyboardMarkupGenerator
     */
    protected $keyboardGenerator;

    /**
     * AbstractMessage constructor.
     *
     * @param TranslatorInterface $translator
     * @param Environment         $twig
     */
    public function __construct(TranslatorInterface $translator, Environment $twig)
    {
        $this->translator = $translator;
        $this->twig = $twig;
        $this->keyboardGenerator = new InlineKeyboardMarkupGenerator($this->translator);
    }

    /**
     * @param array  $parameters
     * @param string $confirmCallbackData
     * @param string $cancelCallbackData
     *
     * @return array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getConfirmationView(array $parameters, string $confirmCallbackData, string $cancelCallbackData): array
    {
        $this->keyboardGenerator->clear();

        $reply = $this->twig->render('confirm.html.twig', $parameters);

        $this->keyboardGenerator->addButton($this->translator->trans('yes'), $confirmCallbackData);
        $this->keyboardGenerator->addButton($this->translator->trans('no'), $cancelCallbackData, true);

        return [$reply, $this->keyboardGenerator->generate()];
    }

    /**
     * Translate a reply
     *
     * @param string $message
     *
     * @return string
     */
    public function getReply(string $message): string
    {
        return $this->translator->trans($message);
    }

    /**
     * Add all the first characters of gyms to the keyboard
     *
     * @param Gym[]  $gyms
     * @param string $callbackData
     */
    protected function addFirstCharacters(array $gyms, string $callbackData): void
    {
        $characters = [];

        $index = 0;
        foreach ($gyms as $gym) {
            $character = mb_substr($gym->getName(), 0, 1);

            if (in_array($character, $characters, true)) {
                continue;
            }

            $characters[] = $character;

            if ($index % 4 === 0) {
                $this->keyboardGenerator->addButton($character, $callbackData . ':' . $character);
            } else {
                $this->keyboardGenerator->addButton($character, $callbackData . ':' . $character, true);
            }

            $index++;
        }
    }
}
