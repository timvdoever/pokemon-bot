<?php

namespace App\Models\Message;

use App\Entity\Team;
use App\Entity\User;
use App\Repository\TeamRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use TelegramBot\Api\Types\User as TelegramUser;
use Twig\Environment;

/**
 * Class TrainerMessage
 */
class TrainerMessage extends AbstractMessage
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TrainerMessage constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param Environment            $twig
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Environment $twig
    ) {
        parent::__construct($translator, $twig);

        $this->userRepository = $entityManager->getRepository(User::class);
        $this->teamRepository = $entityManager->getRepository(Team::class);
    }

    /**
     * Get the main view of the Trainer message flow
     *
     * @return array Sends an array with the reply and an optional keyboard attached
     */
    public function getMainView(): array
    {
        $reply = $this->translator->trans('what_to_update');

        $this->keyboardGenerator->clear();
        $this->keyboardGenerator->addButton(
            'Trainer name',
            'trainer:view:name'
        );
        $this->keyboardGenerator->addButton(
            'Team',
            'trainer:view:team'
        );
        $this->keyboardGenerator->addButton(
            'Level',
            'trainer:view:level',
            true
        );
        $this->keyboardGenerator->addFooter('trainer:delete');

        return [$reply, $this->keyboardGenerator->generate()];
    }

    /**
     * Get a sub view of the Trainer message flow
     *
     * @param string $view
     * @param TelegramUser $user
     *
     * @return array
     */
    public function getSubView(string $view, TelegramUser $user): array
    {
        switch ($view) {
            case 'team':
                return $this->getTrainerTeamView($user);
            case 'name':
                return $this->getTrainerNameView($user);
        }
    }

    /**
     * @param TelegramUser $telegramUser
     *
     * @return array Sends an array with the reply and an optional keyboard attached
     */
    public function getTrainerNameView(TelegramUser $telegramUser): array
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['telegram_id' => $telegramUser->getId()]);

        if (null === $user->getTrainerName()) {
            $reply = $this->translator->trans('no_trainer_name_set');
        } else {
            $reply = $this->translator->trans('trainer_name_set', ['name' => $user->getTrainerName()]);
        }

        return [$reply, null];
    }

    public function getTrainerTeamView(TelegramUser $telegramUser): array
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['telegram_id' => $telegramUser->getId()]);

        if (null === $user->getTeam()) {
            $reply = $this->translator->trans('no_team_set');
        } else {
            $reply = $this->translator->trans('team_set');
        }

        $this->keyboardGenerator->clear();

        $valor = $this->teamRepository->findOneBy(['name' => 'valor']);
        if ($user->getTeam() !== $valor) {
            $this->keyboardGenerator->addButton(
                'Valor ' . Icons::TEAM_VALOR,
                'trainer:update:team:' . $valor->getId()
            );
        }

        $instinct = $this->teamRepository->findOneBy(['name' => 'instinct']);
        if ($user->getTeam() !== $instinct) {
            $this->keyboardGenerator->addButton(
                'Instinct ' . Icons::TEAM_INSTINCT,
                'trainer:update:team:' . $instinct->getId(),
                true
            );
        }

        $mystic = $this->teamRepository->findOneBy(['name' => 'mystic']);
        if ($user->getTeam() !== $mystic) {
            $this->keyboardGenerator->addButton(
                'Mystic ' . Icons::TEAM_MYSTIC,
                'trainer:update:team:' . $mystic->getId(),
                true
            );
        }

        $this->keyboardGenerator->addFooter('trainer:delete', 'trainer:init');

        return [$reply, $this->keyboardGenerator->generate()];
    }
}
