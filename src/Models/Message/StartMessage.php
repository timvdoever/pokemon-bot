<?php

namespace App\Models\Message;

use App\Entity\Gym;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;
use Twig\Environment;

/**
 * Class StartMessage
 */
class StartMessage extends AbstractMessage
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * StartMessage constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param Environment            $twig
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator, Environment $twig)
    {
        parent::__construct($translator, $twig);

        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getMainView(): array
    {
        $reply = $this->translator->trans('pick_gym_letter');
        $gyms = $this->entityManager->getRepository(Gym::class)->findBy(['enabled' => true], ['name' => 'ASC']);
        $this->keyboardGenerator->clear();

        $this->addFirstCharacters($gyms, 'start:view:list');

        $this->keyboardGenerator->addFooter('start:delete');

        return [$reply, $this->keyboardGenerator->generate()];
    }

    /**
     * @param string $view
     * @param string $identifier
     *
     * @return array
     */
    public function getSubView(string $view, string $identifier): array
    {
        $this->keyboardGenerator->clear();

        switch ($view) {
            case 'list':
                return $this->getGymListView($identifier);
        }
    }

    /**
     * Get a list of gyms after picking the first character
     *
     * @param $identifier
     *
     * @return array
     */
    private function getGymListView($identifier): array
    {
        $reply = $this->translator->trans('pick_gym');

        $gyms = $this->entityManager->getRepository(Gym::class)->findByFirstCharacter($identifier);

        foreach ($gyms as $gym) {
            $this->keyboardGenerator->addButton($gym->getName(), 'start:view:level:');
        }

        $this->keyboardGenerator->addFooter('start:delete', 'start:init');

        return [$reply, $this->keyboardGenerator->generate()];
    }
}
