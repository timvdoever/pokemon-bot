<?php

namespace App\Command;

use App\Collector\PokemonDataCollector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class ImportPokemonCommand
 */
class ImportPokemonCommand extends Command
{
    protected static $defaultName = 'app:pokemon:import';

    /**
     * @var PokemonDataCollector
     */
    private $collector;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ImportPokemonCommand constructor.
     *
     * @param PokemonDataCollector   $collector
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(PokemonDataCollector $collector, EntityManagerInterface $entityManager)
    {
        $this->collector = $collector;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    /**
     * Configure the command
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Import Pokemon into the database from a given source')
            ->setHelp('The source can be changed in configuration');
    }

    /**
     * Get all the Pokemon data from the source
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $end = 809;

        $io->text('Starting Pokèmon import...');

        $progress = new ProgressBar($output, $end);
        $pokemonData = $this->collector->collect($end, 1, $progress);
        $progress->finish();

        foreach ($pokemonData as $pokemon) {
            $this->entityManager->persist($pokemon);
        }

        $this->entityManager->flush();

        return 0;
    }
}
