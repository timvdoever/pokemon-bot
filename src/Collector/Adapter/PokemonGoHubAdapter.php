<?php

namespace App\Collector\Adapter;

use App\Entity\Pokemon;
use App\Models\PokemonData;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class PokemonGoHubAdapter
 */
class PokemonGoHubAdapter implements PokemonDataAdapterInterface
{
    private const BASE_URL = 'https://db.pokemongohub.net/api/pokemon/';

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PokemonGoHubAdapter constructor.
     *
     * @param HttpClientInterface $client
     * @param SerializerInterface $serializer
     */
    public function __construct(HttpClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getPokemonData(int $id, string $form = ''): Pokemon
    {
        $requestUrl = self::BASE_URL . $id . $this->getFormQueryString($form);

        try {
            $response = $this->client->request('GET', $requestUrl);

            $data = $response->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $exception) {
            return new Pokemon();
        }

        return $this->hydratePokemonData($data);
    }

    /**
     * Create the query string to append to the url
     *
     * @param string $form
     *
     * @return string
     */
    private function getFormQueryString(string $form): string
    {
        return '?form=' . ucfirst($form);
    }

    /**
     * @param string $data
     *
     * @return PokemonData
     */
    private function hydratePokemonData(string $data): Pokemon
    {
        $data = $this->serializer->decode($data, 'json');

        $pokemon = new Pokemon();
        $pokemon->setPokedexId($data['id']);
        $pokemon->setName($data['name']);
        $pokemon->setForm($data['form'] ?? 'Normal');
        $pokemon->setForms($data['forms']);
        $pokemon->setShiny(0);
        $pokemon->setRaidLevel('0');
        $pokemon->setMinimumCp($data['CPs']['raidCaptureMin']);
        $pokemon->setMaximumCp($data['CPs']['raidCaptureMax']);
        $pokemon->setMinimumWeatherCp($data['CPs']['raidCaptureBoostMin']);
        $pokemon->setMaximumWeatherCp($data['CPs']['raidCaptureBoostMax']);

        return $pokemon;
    }
}
