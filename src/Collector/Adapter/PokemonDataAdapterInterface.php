<?php

namespace App\Collector\Adapter;

use App\Entity\Pokemon;

/**
 * Class PokemonDataAdapterInterface
 */
interface PokemonDataAdapterInterface
{
    /**
     * Get the data from a pokemon and hydrate it into a PokemonData object
     *
     * @param int    $id
     * @param string $form
     *
     * @return Pokemon
     */
    public function getPokemonData(int $id, string $form = 'normal'): Pokemon;
}
