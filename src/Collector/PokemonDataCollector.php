<?php

namespace App\Collector;

use App\Collector\Adapter\PokemonDataAdapterInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PokemonDataCollector
 */
class PokemonDataCollector
{
    /**
     * @var PokemonDataAdapterInterface
     */
    private $adapter;

    /**
     * PokemonDataCollector constructor.
     *
     * @param PokemonDataAdapterInterface $adapter
     */
    public function __construct(PokemonDataAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param int              $end
     * @param int              $start
     * @param ProgressBar|null $progress
     *
     * @return array
     */
    public function collect(int $end, int $start = 1, ProgressBar $progress = null): array
    {
        $pokemonData = [];

        for ($i = $start; $i <= $end; ++$i) {
            $pokemon = $this->getAdapter()->getPokemonData($i);
            $forms = $pokemon->getForms();
            $pokemonData[] = $pokemon;

            if (count($forms) <= 1) {
                continue;
            }

            foreach ($forms as $index => $form) {
                if (0 === $index) {
                    continue;
                }

                $pokemonData[] = $this->getAdapter()->getPokemonData($i, $form['value']);
            }

            if ($progress) {
                $progress->advance();
            }
        }

        return $pokemonData;
    }

    /**
     * @return PokemonDataAdapterInterface
     */
    private function getAdapter(): PokemonDataAdapterInterface
    {
        return $this->adapter;
    }
}
