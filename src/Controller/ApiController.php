<?php

namespace App\Controller;

use App\Collector\PokemonDataCollector;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiController
 */
class ApiController extends AbstractController
{
    /**
     * @var PokemonDataCollector
     */
    private $collector;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ApiController constructor.
     *
     * @param PokemonDataCollector $collector
     * @param SerializerInterface  $serializer
     */
    public function __construct(PokemonDataCollector $collector, SerializerInterface $serializer)
    {
        $this->collector = $collector;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $this->collector->collect(386, 386);

        $json = $this->serializer->serialize($data, 'json');

        return JsonResponse::fromJsonString($json);
    }
}
