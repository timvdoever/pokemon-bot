<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use TelegramBot\Api\Types\User as TelegramUser;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Create a new User from a TelegramUser
     *
     * @param TelegramUser $telegramUser
     *
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(TelegramUser $telegramUser): User
    {
        $user = new User($telegramUser);
        $user->setTrainerName(null);
        $user->setLevel(5);

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        return $this->findOneBy(['telegram_id' => $telegramUser->getId()]);
    }

    /**
     * Update a user or create one if no existing user is found
     *
     * @param TelegramUser $telegramUser
     *
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(TelegramUser $telegramUser): User
    {
        $user = $this->findOneBy(['telegram_id' => $telegramUser->getId()]);

        if (null === $user) {
            $user = $this->create($telegramUser);

            return $user;
        }

        $user->setFirstName($telegramUser->getFirstName());
        $user->setUsername($telegramUser->getUsername());
        $user->setLanguageCode($telegramUser->getLanguageCode());

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        return $user;
    }
}
