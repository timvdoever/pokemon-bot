<?php

namespace App\Repository;

use App\Entity\Gym;
use App\Exception\ObjectExistsException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * @method Gym|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gym|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gym[]    findAll()
 * @method Gym[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GymRepository extends ServiceEntityRepository
{
    /**
     * GymRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gym::class);
    }

    /**
     * Create a new gym in the database.
     * Throw an ObjectExistsException if one already exists by the same name.
     *
     * @param string $name
     * @param string $address
     * @param string $latitude
     * @param string $longitude
     * @param string $imageUrl
     * @param bool   $enabled
     * @param bool   $ex
     *
     * @return Gym
     * @throws ObjectExistsException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(
        string $name,
        string $address,
        string $latitude,
        string $longitude,
        string $imageUrl = '',
        bool $enabled = false,
        bool $ex = false
    ): Gym {
        if ($this->exists($name)) {
            throw new ObjectExistsException('Gym already exists');
        }

        $gym = new Gym();
        $gym->setName($name);
        $gym->setAddress($address);
        $gym->setLatitude($latitude);
        $gym->setLongitude($longitude);
        $gym->setImageUrl($imageUrl);
        $gym->setEnabled($enabled);
        $gym->setEx($ex);

        $this->getEntityManager()->persist($gym);
        $this->getEntityManager()->flush();

        return $this->findOneBy(['name' => $name]);
    }

    /**
     * Check if a gym with the same name already exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function exists(string $name): bool
    {
        $result = $this->createQueryBuilder('g')
            ->where('g.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult();

        return 0 !== count($result);
    }

    /**
     * Find gyms which names start with the given character
     *
     * @param string $character
     * @param bool   $enabled
     *
     * @return array
     */
    public function findByFirstCharacter(string $character, bool $enabled = true): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.name LIKE :name')
            ->andWhere('g.enabled = :enabled')
            ->setParameters([
                'name' => $character . '%',
                'enabled' => $enabled
            ])
            ->getQuery()
            ->getResult();
    }
}
