<?php

namespace App\Telegram\Command;

use App\Exception\MissingMessageException;
use App\Handler\GymInformationHandler;
use App\Repository\GymRepository;
use BoShurik\TelegramBotBundle\Telegram\Command\AbstractCommand;
use BoShurik\TelegramBotBundle\Telegram\Command\PublicCommandInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Update;

/**
 * Class GymCommand
 */
class GymCommand extends AbstractCommand implements PublicCommandInterface
{
    /**
     * @var GymInformationHandler
     */
    private $handler;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $aliases;

    /**
     * GymCommand constructor.
     *
     * @param GymInformationHandler $handler
     * @param string                $description
     * @param array                 $aliases
     */
    public function __construct(GymInformationHandler $handler, string $description = 'Edit a gym', array $aliases = [])
    {
        $this->handler = $handler;
        $this->description = $description;
        $this->aliases = $aliases;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return '/gym';
    }

    /**
     * {@inheritDoc}
     */
    public function execute(BotApi $api, Update $update)
    {
        try {
            $this->handler->init($update);
        } catch (MissingMessageException $exception) {
            $api->sendMessage(
                $update->getMessage()->getFrom()->getId(),
                'Something went wrong, please contact the bot maintainer.'
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
