<?php

namespace App\Telegram\Command;

use App\Handler\TrainerInformationHandler;
use BoShurik\TelegramBotBundle\Telegram\Command\AbstractCommand;
use BoShurik\TelegramBotBundle\Telegram\Command\PublicCommandInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Update;

/**
 * Class TrainerCommand
 */
class TrainerCommand extends AbstractCommand implements PublicCommandInterface
{
    /**
     * @var TrainerInformationHandler
     */
    private $handler;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $aliases;

    /**
     * TrainerCommand constructor.
     *
     * @param TrainerInformationHandler $handler
     * @param string                    $description
     * @param array                     $aliases
     */
    public function __construct(
        TrainerInformationHandler $handler,
        $description = 'Change your trainer information',
        $aliases = ['trainerinfo']
    ) {
        $this->handler = $handler;
        $this->description = $description;
        $this->aliases = $aliases;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return '/trainer';
    }

    /**
     * @inheritDoc
     */
    public function execute(BotApi $api, Update $update): void
    {
        $this->handler->init($update);
    }

    /**
     * {@inheritDoc}
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return $this->description;
    }
}
