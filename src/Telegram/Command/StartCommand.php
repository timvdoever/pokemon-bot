<?php

namespace App\Telegram\Command;

use App\Handler\StartRaidHandler;
use App\Repository\GymRepository;
use BoShurik\TelegramBotBundle\Telegram\Command\AbstractCommand;
use BoShurik\TelegramBotBundle\Telegram\Command\PublicCommandInterface;
use Symfony\Component\Serializer\SerializerInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;
use TelegramBot\Api\Types\Update;

/**
 * Class StartCommand
 */
class StartCommand extends AbstractCommand implements PublicCommandInterface
{
    /**
     * @var StartRaidHandler
     */
    private $handler;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $aliases;

    /**
     * StartCommand constructor.
     *
     * @param StartRaidHandler $handler
     * @param string           $description
     * @param array            $aliases
     */
    public function __construct(
        StartRaidHandler $handler,
        string $description = 'Create a raid',
        array $aliases = ['create']
    ) {
        $this->handler = $handler;
        $this->description = $description;
        $this->aliases = $aliases;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return '/start';
    }

    /**
     * {@inheritDoc}
     */
    public function execute(BotApi $api, Update $update)
    {
        $this->handler->init($update);
    }

    /**
     * {@inheritDoc}
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
