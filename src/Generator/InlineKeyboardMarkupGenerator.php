<?php

namespace App\Generator;

use Symfony\Contracts\Translation\TranslatorInterface;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

/**
 * Class InlineKeyboardMarkupGenerator
 */
class InlineKeyboardMarkupGenerator
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array
     */
    private $inlineKeyboardMarkup;

    /**
     * InlineKeyboardMarkupGenerator constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->inlineKeyboardMarkup = [];
    }

    /**
     * Add a button to the keyboard, optionally to the previous row
     *
     * @param string $text
     * @param string $callbackData
     * @param bool   $addToLastRow
     */
    public function addButton(string $text, string $callbackData, bool $addToLastRow = false): void
    {
        $button = [
            'text' => $this->translator->trans($text),
            'callback_data' => $callbackData
        ];

        if (false === $addToLastRow || 0 === count($this->inlineKeyboardMarkup)) {
            $row[] = $button;
            $this->inlineKeyboardMarkup[] = $row;
        } else {
            $this->inlineKeyboardMarkup[count($this->inlineKeyboardMarkup) - 1][] = $button;
        }
    }

    /**
     * Adds a footer with an abort button and an optional back button
     *
     * @param string $backData
     * @param string $abortData
     */
    public function addFooter(string $abortData, string $backData = null): void
    {
        if (null === $backData) {
            $this->addButton(
                $this->translator->trans('abort'),
                $abortData
            );
        } else {
            $this->addButton(
                $this->translator->trans('back'),
                $backData
            );
            $this->addButton(
                $this->translator->trans('abort'),
                $abortData,
                true
            );
        }
    }

    /**
     * Generate the keyboard and return it
     *
     * @param bool $clear
     *
     * @return InlineKeyboardMarkup
     */
    public function generate(bool $clear = true): InlineKeyboardMarkup
    {
        $keyboard = new InlineKeyboardMarkup($this->inlineKeyboardMarkup);

        if (true === $clear) {
            $this->clear();
        }

        return $keyboard;
    }

    /**
     * Clear the keyboard
     */
    public function clear(): void
    {
        $this->inlineKeyboardMarkup = [];
    }

}
