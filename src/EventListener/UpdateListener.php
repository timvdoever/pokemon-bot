<?php

namespace App\EventListener;

use App\Handler\HandlerInterface;
use BoShurik\TelegramBotBundle\Event\UpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UpdateListener
 */
class UpdateListener implements EventSubscriberInterface
{
    /**
     * @var HandlerInterface
     */
    private $firstHandler;

    /**
     * UpdateListener constructor.
     *
     * @param iterable $handlers
     */
    public function __construct(iterable $handlers)
    {
        $this->firstHandler = $handlers->getIterator()->current();
        $previousHandler = $this->firstHandler;

        foreach ($handlers as $handler) {
            if ($handler === $previousHandler) {
                continue;
            }

            $previousHandler->setNext($handler);
            $previousHandler = $handler;
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            UpdateEvent::class => 'onUpdate',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function onUpdate(UpdateEvent $event): void
    {
        $update = $event->getUpdate();

        $this->firstHandler->handle($update);

        $event->setProcessed();
    }
}
