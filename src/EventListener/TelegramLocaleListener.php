<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\InvalidJsonException;
use TelegramBot\Api\Types\Update;

/**
 * Class TelegramLocaleListener
 */
class TelegramLocaleListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 17]
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        $this->setLocale($request);
    }

    /**
     * @param Request $request
     */
    private function setLocale(Request $request): void
    {
        $locale = $request->getDefaultLocale();

        try {
            if (($content = $request->getContent()) && $data = BotApi::jsonValidate($content, true)) {
                $update = Update::fromResponse($data);

                if ($message = $update->getMessage()) {
                    $locale = $message->getFrom()->getLanguageCode();
                }

                if ($callback = $update->getCallbackQuery()) {
                    $locale = $callback->getFrom()->getLanguageCode();
                }
            }
        } catch (InvalidJsonException $exception) {
        }

        $request->setLocale($locale);
    }
}
