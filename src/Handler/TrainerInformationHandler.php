<?php

namespace App\Handler;

use App\Entity\Team;
use App\Entity\User;
use App\Models\Message\TrainerMessage;
use Doctrine\ORM\EntityManagerInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Update;
use TelegramBot\Api\Types\User as TelegramUser;

/**
 * Class TrainerInformationHandler
 */
class TrainerInformationHandler extends AbstractHandler
{
    /**
     * TrainerInformationHandler constructor.
     *
     * @param BotApi                 $api
     * @param EntityManagerInterface $entityManager
     * @param TrainerMessage         $trainerMessage
     */
    public function __construct(
        BotApi $api,
        EntityManagerInterface $entityManager,
        TrainerMessage $trainerMessage
    ) {
        parent::__construct($api, $entityManager, $trainerMessage);
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if (!$this->isApplicable($update)) {
            return parent::handle($update);
        }

        $action = $this->getCallbackAction($update);
        $reply = null;
        $keyboard = null;
        $user = $update->getCallbackQuery()->getFrom();

        switch ($action) {
            case 'delete':
                $reply = $this->message->getReply('process_aborted');
                $this->delete($update->getCallbackQuery()->getMessage(), $reply);
                return null;
            case 'init':
                [$reply, $keyboard] = $this->message->getMainView();
                break;
            case 'view':
                [$reply, $keyboard] = $this->message->getSubView(
                    $this->getCallbackParameter($update),
                    $user
                );
                break;
            case 'update':
                $reply = $this->updateTrainerInformation(
                    $user,
                    $this->getCallbackParameter($update),
                    $this->getCallbackIdentifier($update)
                );
                break;
        }

        if ($reply) {
            $this->update(
                $update->getCallbackQuery()->getMessage()->getChat()->getId(),
                $update->getCallbackQuery()->getMessage()->getMessageId(),
                $reply,
                $keyboard
            );
        }

        return null;
    }

    /**
     * Update the requested information for this user
     *
     * @param TelegramUser $user
     * @param string       $property
     * @param string       $value
     *
     * @return string
     */
    private function updateTrainerInformation(TelegramUser $user, string $property, string $value): string
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['telegram_id' => $user->getId()]);

        if ('team' === $property) {
            $team = $this->entityManager->getRepository(Team::class)->find((int)$value);
            $user->setTeam($team);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->message->getReply('information_updated');
    }

    /**
     * Check if this handler can handle the update
     */
    private function isApplicable(Update $update): bool
    {
        return $update->getCallbackQuery() && explode(':', $update->getCallbackQuery()->getData())[0] === 'trainer';
    }
}
