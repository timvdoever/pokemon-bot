<?php

namespace App\Handler;

use App\Exception\MissingMessageException;
use App\Models\Message\MessageInterface;
use Doctrine\ORM\EntityManagerInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;
use TelegramBot\Api\Types\Message;
use TelegramBot\Api\Types\Update;

/**
 * Class AbstractBaseHandler
 */
abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @var BotApi
     */
    protected $api;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var MessageInterface|null
     */
    protected $message;

    /**
     * @var HandlerInterface
     */
    private $nextHandler;

    /**
     * AbstractBaseHandler constructor.
     *
     * @param BotApi                 $api
     * @param EntityManagerInterface $entityManager
     * @param MessageInterface       $message
     */
    public function __construct(BotApi $api, EntityManagerInterface $entityManager, MessageInterface $message = null)
    {
        $this->api = $api;
        $this->entityManager = $entityManager;
        $this->message = $message;
    }

    /**
     * {@inheritDoc}
     */
    public function setNext(HandlerInterface $handler): HandlerInterface
    {
        $this->nextHandler = $handler;

        return $handler;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if ($this->nextHandler) {
            $this->nextHandler->handle($update);
        }

        return null;
    }

    /**
     * Initialize a flow
     *
     * @param Update $update
     *
     * @throws MissingMessageException
     */
    public function init(Update $update): void
    {
        if (!$this->message) {
            throw new MissingMessageException(
                'You can not call init if no message was injected into the AbstractHandler'
            );
        }

        [$reply, $keyboard] = $this->message->getMainView();

        $this->reply($update->getMessage()->getFrom()->getId(), $reply, $keyboard);
    }

    /**
     * Send a message with main view
     *
     * @param int                       $chatId
     * @param string                    $reply
     * @param InlineKeyboardMarkup|null $keyboard
     */
    protected function reply(int $chatId, string $reply, ?InlineKeyboardMarkup $keyboard = null): void
    {
        try {
            $this->api->sendMessage($chatId, $reply, 'HTML', true, null, $keyboard);
        } catch (InvalidArgumentException|Exception $exception) {
            return;
        }
    }

    /**
     * Edit a message with updated view
     *
     * @param int                       $chatId
     * @param int                       $messageId
     * @param string                    $reply
     * @param InlineKeyboardMarkup|null $keyboard
     */
    protected function update(int $chatId, int $messageId, string $reply, ?InlineKeyboardMarkup $keyboard = null): void
    {
        $this->api->editMessageText($chatId, $messageId, $reply, 'HTML', true, $keyboard);
    }

    /**
     * Delete a message by optionally editing it or actually deleting it
     *
     * @param Message     $message
     * @param string|null $reply
     */
    protected function delete(Message $message, string $reply = null): void
    {
        if ($reply) {
            $this->api->editMessageText($message->getChat()->getId(), $message->getMessageId(), $reply);
        } else {
            $this->api->deleteMessage($message->getChat()->getId(), $message->getMessageId());
        }
    }

    /**
     * Get the callback action from the data
     *
     * @param Update $update
     *
     * @return string|null
     */
    protected function getCallbackAction(Update $update): ?string
    {
        $data = explode(':', $update->getCallbackQuery()->getData());

        if (count($data) >= 2) {
            return $data[1];
        }

        return null;
    }

    /**
     * Get the callback parameter from the data
     *
     * @param Update $update
     *
     * @return string|null
     */
    protected function getCallbackParameter(Update $update): ?string
    {
        $data = explode(':', $update->getCallbackQuery()->getData());

        if (count($data) >= 3) {
            return $data[2];
        }

        return null;
    }

    /**
     * Get the callback identifier from the data
     * For example a team id, level, raid id etc.
     *
     * @param Update $update
     *
     * @return string|null
     */
    protected function getCallbackIdentifier(Update $update): ?string
    {
        $data = explode(':', $update->getCallbackQuery()->getData());

        if (count($data) >= 4) {
            return $data[3];
        }

        return null;
    }
}
