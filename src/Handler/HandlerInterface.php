<?php

namespace App\Handler;

use TelegramBot\Api\Types\Update;

/**
 * Interface HandlerInterface
 */
interface HandlerInterface
{
    /**
     * Set the next handler in the chain
     *
     * @param HandlerInterface $handler
     *
     * @return HandlerInterface
     */
    public function setNext(HandlerInterface $handler): HandlerInterface;

    /**
     * Handle the incoming data
     *
     * @param Update $update
     *
     * @return string|null
     */
    public function handle(Update $update): ?string;
}
