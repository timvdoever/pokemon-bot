<?php

namespace App\Handler;

use App\Models\Message\StartMessage;
use Doctrine\ORM\EntityManagerInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Update;

/**
 * Class StartRaidHandler
 */
class StartRaidHandler extends AbstractHandler
{
    /**
     * StartRaidHandler constructor.
     *
     * @param BotApi                 $api
     * @param EntityManagerInterface $entityManager
     * @param StartMessage|null      $message
     */
    public function __construct(BotApi $api, EntityManagerInterface $entityManager, StartMessage $message = null)
    {
        parent::__construct($api, $entityManager, $message);
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if (!$this->isApplicable($update)) {
            return parent::handle($update);
        }

        $action = $this->getCallbackAction($update);
        $reply = null;
        $keyboard = null;

        switch ($action) {
            case 'delete':
                $reply = $this->message->getReply('process_aborted');
                $this->delete($update->getCallbackQuery()->getMessage(), $reply);
                return null;
            case 'init':
                [$reply, $keyboard] = $this->message->getMainView();
                break;
            case 'view':
                [$reply, $keyboard] = $this->message->getSubView(
                    $this->getCallbackParameter($update),
                    $this->getCallbackIdentifier($update)
                );
                break;
        }

        if ($reply) {
            $this->update(
                $update->getCallbackQuery()->getMessage()->getChat()->getId(),
                $update->getCallbackQuery()->getMessage()->getMessageId(),
                $reply,
                $keyboard
            );
        }

        return null;
    }

    /**
     * Check if the handler can handle the update
     *
     * @param Update $update
     *
     * @return bool
     */
    private function isApplicable(Update $update): bool
    {
        return $update->getCallbackQuery() && explode(':', $update->getCallbackQuery()->getData())[0] === 'start';
    }
}
