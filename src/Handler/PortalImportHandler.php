<?php

namespace App\Handler;

use App\Entity\Gym;
use App\Exception\ObjectExistsException;
use App\Repository\GymRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Contracts\Translation\TranslatorInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\MessageEntity;
use TelegramBot\Api\Types\Update;

/**
 * Class PortalImportHandler
 */
class PortalImportHandler extends AbstractHandler
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var boolean
     */
    private $importEnabled;

    /**
     * @var boolean
     */
    private $imageEnabled;

    /**
     * @var string
     */
    private $imageDirectory;

    /**
     * PortalImportHandler constructor.
     *
     * @param BotApi                 $api
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param bool                   $importEnabled
     * @param bool                   $imageEnabled
     * @param string                 $imageDirectory
     */
    public function __construct(
        BotApi $api,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        bool $importEnabled,
        bool $imageEnabled,
        string $imageDirectory
    ) {
        parent::__construct($api, $entityManager);

        $this->translator = $translator;
        $this->importEnabled = $importEnabled;
        $this->imageEnabled = $imageEnabled;
        $this->imageDirectory = $imageDirectory;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if (!$this->isApplicable($update)) {
            return parent::handle($update);
        }

        $message = $update->getMessage();

        if (!$this->importEnabled) {
            $this->reply($message->getChat()->getId(), $this->translator->trans('import_disabled'));

            return null;
        }

        $text = $message->getText();
        /** @var MessageEntity[] $entities */
        $entities = $message->getEntities();
        $parts = explode(PHP_EOL, $text);

        $name = $this->extractName($parts);
        $address = $this->extractAddress($parts);
        [$latitude, $longitude] = $this->extractCoordinates($entities[1]);
        $imageUrl = $this->extractImage($entities[0], $name, $this->imageEnabled);

        try {
            $this->entityManager->getRepository(Gym::class)->create($name, $address, $latitude, $longitude, $imageUrl);

            $reply = 'gym_added';
        } catch (ObjectExistsException $exception) {
            $reply = 'gym_exists';
        } catch (ORMException|OptimisticLockException $exception) {
            $reply = 'error_adding_gym';
        }

        $this->reply($message->getChat()->getId(), $this->translator->trans($reply));

        return null;
    }

    /**
     * Extract the gym name from the first line of the message
     *
     * @param array $parts
     *
     * @return string
     */
    private function extractName(array $parts): string
    {
        $name = mb_substr($parts[0], 1, -mb_strlen('(Intel)'));

        return trim($name);
    }


    /**
     * Extract and format the address from the fifth line of the message
     *
     * @param array $parts
     *
     * @return string
     */
    private function extractAddress(array $parts): string
    {
        $trimmedAddress = trim($parts[4]);
        $addressParts = explode(',', $trimmedAddress, -1);
        $address = implode(',', $addressParts);

        return trim($address);
    }

    /**
     * Extract the coordinates from the entities array
     *
     * @param MessageEntity $entity
     *
     * @return array
     */
    private function extractCoordinates(MessageEntity $entity): array
    {
        $parts = explode('&pll=', $entity->getUrl());

        return explode(',', $parts[1]);
    }

    /**
     * Download the image from the web
     *
     * @param MessageEntity $entity
     * @param string        $name
     * @param bool          $saveLocally
     *
     * @return string|null
     */
    private function extractImage(MessageEntity $entity, string $name, bool $saveLocally): ?string
    {
        if ($entity->getOffset() === 0 && $entity->getLength() === 1) {
            $url = $entity->getUrl();

            if (!$saveLocally) {
                return $url;
            }

            $filename = strtolower(str_replace(' ', '_', $name)) . '.png';
            $filepath = $this->imageDirectory . DIRECTORY_SEPARATOR . $filename;
            $image = file_get_contents($url);
            file_put_contents($filepath, $image);

            return $filepath;
        }

        return null;
    }

    /**
     * Check if this handler can handle the update
     *
     * @param Update $update
     *
     * @return bool
     */
    private function isApplicable(Update $update): bool
    {
        if (!$update->getMessage()) {
            return false;
        }

        $portalName = strtok($update->getMessage()->getText(), PHP_EOL);

        return substr_compare($portalName, '(Intel)', -mb_strlen('(Intel)')) === 0;
    }
}
