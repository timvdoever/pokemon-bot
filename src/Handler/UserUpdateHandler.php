<?php

namespace App\Handler;

use App\Entity\User;
use TelegramBot\Api\Types\Update;

/**
 * Class UserUpdateHandler
 */
class UserUpdateHandler extends AbstractHandler
{
    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if (!$this->isApplicable($update)) {
            return parent::handle($update);
        }

        $user = null;

        if ($message = $update->getMessage()) {
            $user = $message->getFrom();
        } elseif ($callback = $update->getCallbackQuery()) {
            $user = $callback->getFrom();
        }

        if (null === $user) {
            return null;
        }

        try {
            $this->entityManager->getRepository(User::class)->update($user);
        } catch (\Exception $exception) {
            $this->api->sendMessage(
                $user->getId(),
                'Could not update your user information, contact the bot maintainer.'
            );
        }

        return null;
    }

    /**
     * @param Update $update
     *
     * @return bool
     */
    private function isApplicable(Update $update): bool
    {
        return $update->getMessage() || $update->getCallbackQuery();
    }
}
