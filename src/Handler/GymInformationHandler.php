<?php

namespace App\Handler;

use App\Entity\Gym;
use App\Models\Message\GymMessage;
use Doctrine\ORM\EntityManagerInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Update;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class GymInformationHandler
 */
class GymInformationHandler extends AbstractHandler
{
    /**
     * GymInformationHandler constructor.
     *
     * @param BotApi                 $api
     * @param EntityManagerInterface $entityManager
     * @param GymMessage             $message
     */
    public function __construct(BotApi $api, EntityManagerInterface $entityManager, GymMessage $message)
    {
        parent::__construct($api, $entityManager, $message);
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Update $update): ?string
    {
        if (!$this->isApplicable($update)) {
            return parent::handle($update);
        }

        $action = $this->getCallbackAction($update);
        $reply = null;
        $keyboard = null;

        switch ($action) {
            case 'delete':
                $reply = $this->message->getReply('process_aborted');
                $this->delete($update->getCallbackQuery()->getMessage(), $reply);
                return null;
            case 'init':
                [$reply, $keyboard] = $this->message->getMainView();
                break;
            case 'view':
                [$reply, $keyboard] = $this->message->getSubView(
                    $this->getCallbackParameter($update),
                    $this->getCallbackIdentifier($update)
                );
                break;
            case 'update':
                [$reply, $keyboard] = $this->updateGymInformation(
                    $this->getCallbackParameter($update),
                    $this->getCallbackIdentifier($update)
                );
                break;
        }

        if ($reply) {
            $this->update(
                $update->getCallbackQuery()->getMessage()->getChat()->getId(),
                $update->getCallbackQuery()->getMessage()->getMessageId(),
                $reply,
                $keyboard
            );
        }

        return null;
    }

    /**
     * @param string $parameter
     * @param string $identifier
     *
     * @return array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function updateGymInformation(string $parameter, string $identifier): array
    {
        $gym = $this->entityManager->getRepository(Gym::class)->find($identifier);

        [$property, $value] = explode('_', $parameter);

        if ('drop' === $property) {
            if ('confirm' === $value)
                return $this->message->getConfirmationView(
                    [
                        'action' => 'confirm_remove_gym',
                        'gym'    => $gym->getName()
                    ],
                    'gym:update:drop_true:' . $gym->getId(),
                    'gym:update:drop_false:' . $gym->getId()
                );
            elseif ('true' === $value) {
                $this->entityManager->remove($gym);
                $this->entityManager->flush();

                return [$this->message->getReply('gym_deleted'), null];
            } else {
                return [$this->message->getReply('process_aborted'), null];
            }
        }

        if ('enabled' === $property) {
            $gym->setEnabled($value === 'true');
        }

        if ('ex' === $property) {
            $gym->setEx(($value === 'true'));
        }

        $this->entityManager->persist($gym);
        $this->entityManager->flush();

        return $this->message->getSubView('detail', $gym->getId());
    }

    /**
     * Check if this handler can handle the update
     */
    private function isApplicable(Update $update): bool
    {
        return $update->getCallbackQuery() && explode(':', $update->getCallbackQuery()->getData())[0] === 'gym';
    }
}
