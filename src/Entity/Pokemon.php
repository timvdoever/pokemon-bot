<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PokemonRepository")
 */
class Pokemon
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $pokedex_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $form;

    /**
     * @var array
     */
    private $forms;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $raid_level;

    /**
     * @ORM\Column(type="integer")
     */
    private $minimum_cp;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximum_cp;

    /**
     * @ORM\Column(type="integer")
     */
    private $minimum_weather_cp;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximum_weather_cp;

    /**
     * @ORM\Column(type="smallint")
     */
    private $shiny;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPokedexId(): ?int
    {
        return $this->pokedex_id;
    }

    public function setPokedexId(int $pokedex_id): self
    {
        $this->pokedex_id = $pokedex_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getForm(): ?string
    {
        return $this->form;
    }

    public function setForm(string $form): self
    {
        $this->form = $form;

        return $this;
    }

    public function setForms(array $forms): self
    {
        $this->forms = $forms;

        return $this;
    }

    public function getForms(): array
    {
        return $this->forms;
    }

    public function getRaidLevel(): ?string
    {
        return $this->raid_level;
    }

    public function setRaidLevel(string $raid_level): self
    {
        $this->raid_level = $raid_level;

        return $this;
    }

    public function getMinimumCp(): ?int
    {
        return $this->minimum_cp;
    }

    public function setMinimumCp(int $minimum_cp): self
    {
        $this->minimum_cp = $minimum_cp;

        return $this;
    }

    public function getMaximumCp(): ?int
    {
        return $this->maximum_cp;
    }

    public function setMaximumCp(int $maximum_cp): self
    {
        $this->maximum_cp = $maximum_cp;

        return $this;
    }

    public function getMinimumWeatherCp(): ?int
    {
        return $this->minimum_weather_cp;
    }

    public function setMinimumWeatherCp(int $minimum_weather_cp): self
    {
        $this->minimum_weather_cp = $minimum_weather_cp;

        return $this;
    }

    public function getMaximumWeatherCp(): ?int
    {
        return $this->maximum_weather_cp;
    }

    public function setMaximumWeatherCp(int $maximum_weather_cp): self
    {
        $this->maximum_weather_cp = $maximum_weather_cp;

        return $this;
    }

    public function getShiny(): ?int
    {
        return $this->shiny;
    }

    public function setShiny(int $shiny): self
    {
        $this->shiny = $shiny;

        return $this;
    }
}
