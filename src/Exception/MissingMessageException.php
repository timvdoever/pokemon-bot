<?php

namespace App\Exception;

/**
 * Class MissingMessageException
 */
class MissingMessageException extends \Exception
{
}
