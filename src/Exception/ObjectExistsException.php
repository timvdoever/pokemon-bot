<?php

namespace App\Exception;

/**
 * Class ObjectExistsException
 */
class ObjectExistsException extends \Exception
{
}
